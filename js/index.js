/* last update 2020/12/14 12:39 */
jQuery(function($) {
    // js sdk
    const baseUrl = 'https://campaign.giorgioarmanibeauty.cn', // 正式地址
        currentUrl = window.location.href; // 当前地址
    let userinfo;
    // 是否存有userinfo
    if(JSON.parse(sessionStorage.getItem('userinfo')) != null && JSON.parse(sessionStorage.getItem('userinfo')) != undefined) {
        // 有info
        userinfo = JSON.parse(sessionStorage.getItem('userinfo'));
        $('.modal-wel').fadeIn(100);
    } else {
        // 没有info
        if(wxr_getParameterByName('token')) {
            // 带有 token 
            let token = wxr_getParameterByName('token');
            userinfo = getInfo(token);
            sessionStorage.setItem('userinfo', JSON.stringify(userinfo));
            history.pushState({ url: currentUrl, title: document.title }, document.title, currentUrl);
            $('.modal-wel').fadeIn(100);
        } else {
            // 没有 token
            // window.location.href = baseUrl + '/minifeedback/api/Server/Oauth?siteurl=' + currentUrl;
            $('.modal-wel').fadeIn(100);
        };
    };

    // get param
    function wxr_getParameterByName(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        let regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
            results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };

    // get info
    function getInfo(token) {
        let info;
        $.ajax({
            type: 'POST',
            url: baseUrl + '/minifeedback/api/Server/GetInfo',
            cache: false,
            async: false,
            dataType: 'json',
            data: {
                token: token
            },
            success: function(data) {
                // console.log(data);
                info = data.data;
            }
        });
        return info;
    };

    // brower
    const u = navigator.userAgent,
        isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
    let isFocus = 0; // 默认没有在输入

    // ios
    if(isiOS) {
        // wechat
        if(u.toLowerCase().match(/MicroMessenger/i) == 'micromessenger') {
            const H = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
            // 
            setTimeout(function() {
                if($('.wrapper').is(':hidden')) {
                    if($(document).scrollTop() > 0) {
                        if(isFocus == 0) {
                            $('html, body').stop(true, true).animate({scrollTop: 0}, 100, function() {});
                        };
                    };
                };
            }, 500);
            let distance, // 卷起的高度 = 页面实际高度 - 浏览器可视区域高度
                getH = setInterval(function() {
                if($('.wrapper').height() > 0) {
                    distance = $('.wrapper').height() - H;
                    clearInterval(getH);
                    //解决弹出输入法页面无法收回的问题
                    $(document).on('focusin', function(e) {      
                        e.stopPropagation();
                        // console.log('弹出 ' + $(document).scrollTop());
                        isFocus = 1;
                    });
                    $(document).on('focusout', function(e) {
                        e.stopPropagation();
                        // console.log('收起 ' + $(document).scrollTop());
                        isFocus = 0;
                        if($(document).scrollTop() > (distance)) {
                            setTimeout(function() {
                                if(isFocus == 0) {
                                    // console.log('执行收起' + distance);
                                    $('html, body').stop(true, true).animate({scrollTop: distance}, 100, function() {});
                                } else {
                                    // console.log('不执行收起');
                                };
                            }, 100);
                        };
                    });
                    let resizeH;
                    $(document).on('scroll', function() {
                        clearTimeout(resizeH);
                        resizeH = setTimeout(function() {
                            if($('.wrapper').is(':hidden')) {
                                if($(document).scrollTop() > 0) {
                                    if(isFocus == 0) {
                                        $('html, body').stop(true, true).animate({scrollTop: 0}, 100, function() {});
                                    };
                                };
                            } else {
                                if($(document).scrollTop() > (distance)) {
                                    if(isFocus == 0) {
                                        $('html, body').stop(true, true).animate({scrollTop: distance}, 100, function() {});
                                    };
                                };
                            };
                        }, 100);
                    })
                };
            }, 300);
        };
    };

    // modal wel
    $('.modal-wel .btn-start').on('click', function() {
        $('.modal-wel').fadeOut(100, function() {
            $('.container .wrapper').fadeIn(100);
        });
    });

    // buy time
    $('.userTimeVal').on('change propertychange', function() {
    	$('.userTimeVal-copy').text($(this).val().replace(/(\d{4}).(\d{1,2}).(\d{1,2})/mg, '$1年$2月$3日'));
    });

    // counter data
    const counterArr = [
        {
            id: 0,
            value: '杭州',
            childs: [
                {id: 0, value: '杭州银泰'},
                {id: 1, value: '杭州银泰IN77'},
                {id: 2, value: '杭州大厦'},
                {id: 3, value: '杭州银隆'}
            ]
        },
        {
            id: 1,
            value: '宁波',
            childs: [
                {id: 0, value: '宁波银泰'}
            ]
        },
        {
            id: 2,
            value: '上海',
            childs: [
                {id: 0, value: '上海大丸'},
                {id: 1, value: '上海百盛'},
                {id: 2, value: '上海梅陇镇'},
                {id: 3, value: '上海八佰伴'},
                {id: 4, value: '上海久光'},
                {id: 5, value: '上海恒隆港汇'},
                {id: 6, value: '市百一店'},
                {id: 7, value: '上海国金'}
            ]
        },
        {
            id: 3,
            value: '北京',
            childs: [
                {id: 0, value: '北京新光'},
                {id: 1, value: '北京百盛'},
                {id: 2, value: '北京双安'},
                {id: 3, value: '北京君太'},
                {id: 4, value: '北京汉光'},
                {id: 5, value: '北京新世界'},
                {id: 6, value: '北京王府井'},
                {id: 7, value: '北京国贸'},
                {id: 8, value: '北京朝阳大悦城'}
            ]
        },
        {
            id: 4,
            value: '成都',
            childs: [
                {id: 0, value: '成都王府井'},
                {id: 1, value: '成都连卡佛'},
                {id: 2, value: '成都王府井2'},
                {id: 3, value: '成都群光'},
                {id: 4, value: '成都仁和光华'},
                {id: 5, value: '成都伊势丹'},
                {id: 6, value: '成都银泰'}
            ]
        },
        {
            id: 5,
            value: '南京',
            childs: [
                {id: 0, value: '南京中央'},
                {id: 1, value: '南京德基广场'},
                {id: 2, value: '南京新百'},
                {id: 3, value: '南京河西金鹰'},
                {id: 4, value: '南京金鹰'}
            ]
        },
        {
            id: 6,
            value: '深圳',
            childs: [
                {id: 0, value: '深圳COCO PARK'},
                {id: 1, value: '深圳万象天地'},
                {id: 2, value: '深圳海岸城'},
                {id: 3, value: '深圳海雅'},
                {id: 4, value: '深圳益田假日'},
                {id: 5, value: '深圳茂业'}
            ]
        },
        {
            id: 7,
            value: '西安',
            childs: [
                {id: 0, value: '西安SKP'},
                {id: 1, value: '西安大融城'},
                {id: 2, value: '西安开元'},
                {id: 3, value: '西安王府井'},
                {id: 4, value: '西安赛格'},
                {id: 5, value: '西安金鹰'}
            ]
        },
        {
            id: 8,
            value: '重庆',
            childs: [
                {id: 0, value: '重庆世纪新都'},
                {id: 1, value: '重庆协信星光'},
                {id: 2, value: '重庆新光'},
                {id: 3, value: '重庆茂业'},
                {id: 4, value: '重庆远东'}
            ]
        },
        {
            id: 9,
            value: '长沙',
            childs: [
                {id: 0, value: '长沙友谊'},
                {id: 1, value: '长沙国金'},
                {id: 2, value: '长沙平和堂'},
                {id: 3, value: '长沙海信'},
                {id: 4, value: '长沙王府井'}
            ]
        },
        {
            id: 10,
            value: '广州',
            childs: [
                {id: 0, value: '广州友谊'},
                {id: 1, value: '广州天河城'},
                {id: 2, value: '广州广百'},
                {id: 3, value: '广州新鸿基'}
            ]
        },
        {
            id: 11,
            value: '武汉',
            childs: [
                {id: 0, value: '武汉武商'},
                {id: 1, value: '武汉群光'},
                {id: 2, value: '武汉K11'}
            ]
        },
        {
            id: 12,
            value: '沈阳',
            childs: [
                {id: 0, value: '沈阳中兴'},
                {id: 1, value: '沈阳新一城'},
                {id: 2, value: '沈阳新玛特'},
                {id: 3, value: '沈阳万象城'},
            ]
        },
        {
            id: 13,
            value: '石家庄',
            childs: [
                {id: 0, value: '石家庄先天下'},
                {id: 1, value: '石家庄北国'},
                {id: 2, value: '石家庄新百'}
            ]
        },
        {
            id: 14,
            value: '福州',
            childs: [
                {id: 0, value: '福州东百'},
                {id: 1, value: '福州苏宁'}
            ]
        },
        {
            id: 15,
            value: '西宁',
            childs: [
                {id: 0, value: '西宁大象城'},
                {id: 1, value: '西宁王府井'}
            ]
        },
        {
            id: 16,
            value: '贵阳',
            childs: [
                {id: 0, value: '贵阳南国花锦'},
                {id: 1, value: '贵阳国贸'},
                {id: 2, value: '贵阳星力'},
                {id: 3, value: '贵阳逸天城'}
            ]
        },
        {
            id: 17,
            value: '郑州',
            childs: [
                {id: 0, value: '郑州丹尼斯'},
                {id: 1, value: '郑州大卫城'},
                {id: 2, value: '郑州正弘城'}
            ]
        },
        {
            id: 18,
            value: '青岛',
            childs: [
                {id: 0, value: '青岛啤酒城'},
                {id: 1, value: '青岛海信'}
            ]
        },
        {
            id: 19,
            value: '长春',
            childs: [
                {id: 0, value: '长春卓展'},
                {id: 1, value: '长春欧亚'}
            ]
        },
        {
            id: 20,
            value: '乌鲁木齐',
            childs: [
                {id: 0, value: '乌鲁木齐天山百货'},
                {id: 1, value: '乌鲁木齐汇嘉'}
            ]
        },
        {
            id: 21,
            value: '兰州',
            childs: [
                {id: 0, value: '兰州中心'},
                {id: 1, value: '兰州国芳'}
            ]
        },
        {
            id: 22,
            value: '保定',
            childs: [
                {id: 0, value: '保百购物广场'}
            ]
        },
        {
            id: 23,
            value: '东莞',
            childs: [
                {id: 0, value: '东莞国贸'}
            ]
        },
        {
            id: 24,
            value: '南宁',
            childs: [
                {id: 0, value: '南宁百盛'}
            ]
        },
        {
            id: 25,
            value: '南昌',
            childs: [
                {id: 0, value: '南昌百盛'}
            ]
        },
        {
            id: 26,
            value: '南通',
            childs: [
                {id: 0, value: '南通八佰伴'}
            ]
        },
        {
            id: 27,
            value: '厦门',
            childs: [
                {id: 0, value: '厦门SM'},
                {id: 1, value: '厦门中华城'}
            ]
        },
        {
            id: 28,
            value: '合肥',
            childs: [
                {id: 0, value: '合肥滨湖银泰'},
                {id: 1, value: '合肥银泰'}
            ]
        },
        {
            id: 29,
            value: '呼和浩特',
            childs: [
                {id: 0, value: '呼和浩特振华'}
            ]
        },
        {
            id: 30,
            value: '唐山',
            childs: [
                {id: 0, value: '唐山振华'}
            ]
        },
        {
            id: 31,
            value: '嘉兴',
            childs: [
                {id: 0, value: '嘉兴八佰伴'}
            ]
        },
        {
            id: 32,
            value: '大连',
            childs: [
                {id: 0, value: '大连麦凯乐'}
            ]
        },
        {
            id: 33,
            value: '天津',
            childs: [
                {id: 0, value: '天津大悦城'},
                {id: 1, value: '天津海信'}
            ]
        },
        {
            id: 34,
            value: '太原',
            childs: [
                {id: 0, value: '太原王府井'}
            ]
        },
        {
            id: 35,
            value: '宜昌',
            childs: [
                {id: 0, value: '宜昌国贸'}
            ]
        },
        {
            id: 36,
            value: '常州',
            childs: [
                {id: 0, value: '常州新世纪'},
                {id: 1, value: '常州泰富'}
            ]
        },
        {
            id: 37,
            value: '徐州',
            childs: [
                {id: 0, value: '徐州金鹰'}
            ]
        },
        {
            id: 38,
            value: '无锡',
            childs: [
                {id: 0, value: '无锡八佰伴'},
                {id: 1, value: '无锡商业大厦'}
            ]
        },
        {
            id: 39,
            value: '昆山',
            childs: [
                {id: 0, value: '昆山金鹰'}
            ]
        },
        {
            id: 40,
            value: '洛阳',
            childs: [
                {id: 0, value: '洛阳王府井'}
            ]
        },
        {
            id: 41,
            value: '济南',
            childs: [
                {id: 0, value: '济南银座'}
            ]
        },
        {
            id: 42,
            value: '温州',
            childs: [
                {id: 0, value: '温州银泰'}
            ]
        },
        {
            id: 43,
            value: '湖州',
            childs: [
                {id: 0, value: '湖州浙北'}
            ]
        },
        {
            id: 44,
            value: '烟台',
            childs: [
                {id: 0, value: '烟台振华'}
            ]
        },
        {
            id: 45,
            value: '盐城',
            childs: [
                {id: 0, value: '盐城金鹰'}
            ]
        },
        {
            id: 46,
            value: '荆门',
            childs: [
                {id: 0, value: '荆门东方'}
            ]
        },
        {
            id: 47,
            value: '银川',
            childs: [
                {id: 0, value: '银川新华'}
            ]
        },
        {
            id: 48,
            value: '昆明',
            childs: [
                {id: 0, value: '昆明百盛'}
            ]
        },
        {
            id: 49,
            value: '哈尔滨',
            childs: [
                {id: 0, value: '哈尔滨卓展'},
                {id: 1, value: '哈尔滨松雷'},
                {id: 2, value: '哈尔滨远大'}
            ]
        }
    ];

    // counter
    let counterSelect = new MobileSelect({
        trigger: '.userCounterVal',
        title: '请选择柜台',
        wheels: [
                    {
                        data: [
                            {
                                id: '', 
                                value: '',
                                childs: [
                                    {id: '', value: ''},
                                ]
                            }
                        ]
                    }
                ],
        transitionEnd: function(indexArr, data) {
            // console.log(data);
        },
        callback: function(indexArr, data) {
            $('.userCounterVal').text(data[1].value);
        }
    });
    counterSelect.updateWheels(counterArr); // Update the wheel

    // base64
    function imgToBase64(file, callBack) {
        let size = file.size,
            url,
            maxLen,
            base64,
            img = new Image(),
            reader = new FileReader();//读取客户端上的文件
        reader.readAsDataURL(file);
        reader.onload = function() {
            url = reader.result; // 转成base64的结果
            img.src = url;
        };
        img.onload = function() {
            if(size > 0 && size <= 1024 * 500) {
                // console.log('小于500k 不压缩');
                base64 = url;
                callBack(base64);
                return;
            } else if(size > 1024 * 500 && size <= 1024 * 1024) {
                // console.log('大于500k 小于1M');
                maxLen = img.width * 0.85;
            } else if(size > 1024 * 1024 && size <= 1024 * 1024 * 1.5) {
                // console.log('大于1M 小于1.5M');
                maxLen = img.width * 0.75;
            } else {
                // console.log('大于1.5M');
                maxLen = 750;
            };
            // 生成比例
            let width = img.width, 
                height = img.height,
                rate = 1; // 计算缩放比例
            if(width >= height) {
                if(width > maxLen) {
                    rate = maxLen / width;
                };
            } else {
                if(height > maxLen) {
                    rate = maxLen / height;
                };
            };
            img.width = width * rate;
            img.height = height * rate;
            // 生成canvas
            let canvas = document.createElement('canvas'),
                ctx = canvas.getContext('2d');
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img, 0, 0, img.width, img.height);
            base64 = canvas.toDataURL('image/jpeg', 0.9);
            callBack(base64);
        };
    };

    // upload
    const rep = /^image\/(jpe?g|png|bmp|tiff)$/i; // 格式
    let ticket = ''; // 图片
    $('.inputfile').on('change propertychange', function() {
        let file = this.files[0];
        if(file != undefined || file != null) {
            if(rep.test(file.type)) {
                // console.log('可以上传');
                $('.box-upload .box-loading').fadeIn(100);
                imgToBase64(file, function (base64) {
                    // console.log(base64);
                    $('.preview').attr('src', base64);
                    ticket = base64;
                });
                $('.box-upload .box-loading').fadeOut(100);
            } else {
                alert('图片格式不正确，请上传 jpg|jpeg|png|bmp|tiff 格式的图片');
            };
        };
    });

    // type
    $('.type-item label').on('click', function() {
        if($(this).next().prop('checked')) {
            $(this).find('img').attr('src', 'img/' + $(this).next().attr('id') + 0 + '.png');
        } else {
            $(this).find('img').attr('src', 'img/' + $(this).next().attr('id') + 1 + '.png');
        };
    });
	
	// check name
    function checkName(name) {
        let reg = /^[\u4e00-\u9fa5a-zA-Z]{2,16}$/;
        if(name == '') {
            alert('请输入姓名');
            return false;
        } else if(!reg.test(name)) {
            alert('名字格式不正确 请勿输入除中英文以外的字符');
            return false;
        } else {
            return true;
        };
    };

    // check number
    function checkNumber(number) {
        if(number == '') {
            alert('请输入手机号');
            return false;
        } else if(!/^1[0-9][0-9]\d{8}$/.test(number)) {
            alert('请输入正确的电话号码');
            return false;
        } else {
            return true;
        };
    };

    // check code
    function checkCode(code, length) {
        if(code == '') {
            alert('请输入验证码');
            return false;
        } else if(code.length != length){
            alert('请输入' + length + '位验证码');
            return false;
        } else {
            return true;
        };
    };

    // check buy time
    function checkTime(time) {
        if(time == undefined || time == null || time == '') {
            alert('请选择购买时间');
            return false;
        } else {
            return true;
        };
    };

    // check counter
    function checkCounter(counter) {
        if(counter == undefined || counter == null || counter == '') {
            alert('请选择购买柜台');
            return false;
        } else {
            return true;
        };
    };

    // check upload
    function checkUpload(file) {
        if(file == '') {
            alert('请上传购买凭证');
            return false;
        } else {
            return true;
        };
    };

    // check type
    function checkType(type) {
        if(type == undefined || type == null) {
            alert('请选择反馈类型');
            return false;
        } else {
            return true;
        };
    };

    // btn send 
    $('.btn-send').off('click');
    $('.btn-send').on('click', function() {
        if(checkNumber($('#userPhoneVal').val())) {
            $('.btn-send').hide();
            timeCountDown($('.seconds'), 60);
            getCode();
        };
    });

    // count down
    function timeCountDown(elm, t) {
        elm.show();
        elm.text(t);
        let timeCountDownFun = setInterval(function() {
            if(t == 0) {
                elm.hide();
                $('.btn-send').show();
                elm.text('');
                clearInterval(timeCountDownFun);
                // console.log('time countdown done');
            } else {
                t--;
                elm.text(t);
            }
        }, 1000);
    };

    // get code
    function getCode() {
        $.ajax({
            type: 'POST',
            url: baseUrl + '/minifeedback/api/Server/SendSms',
            cache: false,
            // async: false,
            dataType: 'json',
            data: {
                'Mobile': $('#userPhoneVal').val(),
                'openId': userinfo.data,
                'Sign': userinfo.sign
            },
            success: function(data) {
                // console.log(data);
                alert(data.msg);
            },
            error: function() {
                // 
                alert('网络错误，请刷新重试');
            },
            complete: function(data) {
                // todo
            }
        });
    };

    // type arr
    function typeFun() {
        let arr = [],
            len = $('.box-type input:checked').length;
        if(len > 0) {
            for(let i = 0; i < len; i++) {
                arr.push($($('.box-type input:checked')[i]).val());
            };
        };
        return arr.join(',');
    };

    // submit
    let canSubmit = 1; // 是否可以点击提交，默认可以
    $('.btn-submit').off('click');
    $('.btn-submit').on('click', function() {
        // console.log($('#userNameVal').val());
        // console.log($('#userPhoneVal').val());
        // console.log($('#userCodeVal').val());
        // console.log($('#userTimeVal').val());
        // console.log(counterSelect.getValue()[1].value);
        // console.log(ticket);
        // console.log(typeFun());
        // console.log($('.fb').val());
        if(checkName($('#userNameVal').val()) && checkNumber($('#userPhoneVal').val()) && checkCode($('#userCodeVal').val(), 6) && checkTime($('#userTimeVal').val()) && checkCounter(counterSelect.getValue()) && checkUpload(ticket) && checkType($('.box-type input:checked').val())) {
            // console.log($('#userNameVal').val());
            // console.log($('#userPhoneVal').val());
            // console.log($('#userCodeVal').val());
            // console.log($('#userTimeVal').val());
            // console.log(counterSelect.getValue()[1].value);
            // console.log(ticket);
            // console.log(typeFun());
            // console.log($('.fb').val());
            let type = typeFun();
            if(canSubmit == 0) {
                // 正在调接口，不可以重复调取
                return;
            };
            canSubmit = 0;
            $('.box-submit .box-loading').removeClass('hide');
            // 同意
            $.ajax({
            	type: 'POST',
            	url: baseUrl + '/minifeedback/api/Server/Save',
            	// async: false,
                cache: false,
                dataType: 'json',
                data: {
                    'Name': $('#userNameVal').val(),
                    'Mobile': $('#userPhoneVal').val(),
                    'SmsCode': $('#userCodeVal').val(),
                    'ByTime': $('#userTimeVal').val(),
                    'Counter': counterSelect.getValue()[1].value,
                    'Base64ImgStr': ticket,
                    'Type': type,
                    'Content': $('.fb').val()
                },
                success: function(data) {
                    // console.log(data);
                    if(data.code == 0) {
                        $('.container .wrapper').fadeOut(100, function() {
                            $('.modal-success').fadeIn(100);
                        });
                    };
                    if(data.code == 1) {
                        alert(data.msg);
                    };
                },
                error: function() {
                    //
                    alert('网络错误，请刷新重试');
                },
                complete: function(data) {
                    //
                    canSubmit = 1;
                    $('.box-submit .box-loading').addClass('hide');
                }
            });
        };
    });
});
